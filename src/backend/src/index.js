const _ = require('lodash');
const express = require('express');
const app = require('express')();
const path = require('path');
const http = require('http').Server(app);
const io = require('socket.io')(http);
//------
const User = require('./models/User');
const UserStorage = require('./models/UserStorage');
const Logger = require('./utils/Logger');
//------
const userStorage = new UserStorage();
//------

app.use('/', express.static(
    path.resolve(__dirname, '../../frontend')
));
//------

io.on('connection', function(socket) {
    Logger.connected(socket.id);
    socket.emit('imneedsize', {});
    const user = new User(socket.id);


    userStorage.add(user);
    if (userStorage.first().id === socket.id){
        io.to(userStorage.first().id).emit('show', 'for your eyes only');
    }

    socket.on('click', (msg) => {
        Logger.click(socket.id);
        if (userStorage.first().id === socket.id){
            socket.emit('start', 'Run, Forest, run!');
            this.nextUser = userStorage.first();
        }
    });

    socket.on('mysize', function(msg) {
        Logger.log(socket.id + " " + JSON.stringify(msg));
    });

    let nextUser;
    socket.on('wentOverTheEdge', (msg) => {
        Logger.wentOverTheEdge(socket.id, msg);
        this.nextUser = userStorage.getNextUser(socket.id);
        Logger.log(JSON.stringify(nextUser));
        if (this.nextUser){
            io.to(this.nextUser.id).emit('show', { type: 'right', c: msg.c });
        }
    });

    socket.on('disconnect', () => {
        if (this.nextUser && this.nextUser.id === socket.id){
            this.nextUser = userStorage.getNextUser(socket.id);
        }
        userStorage.removeById(socket.id);
        Logger.disconnected(socket.id);
        if (this.nextUser){
            io.to(this.nextUser.id).emit('start', { type: 'right', c: 0 });
        }
    });
});

let port = 4000;
http.listen(port, Logger.log('Cockroach antennae is on fire on ' + port + ' port!'));
