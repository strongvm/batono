const _ = require('lodash');
const Logger = require('../utils/Logger');

class UserStorage {
    constructor() {
        this.users = [];
    }

    getAll() {
        return this.users;
    }

    getById(userId) {
        return _.find(this.users, { id: userId });
    }

    getNextUser(userId){
        let ind = this.users.findIndex(user => user.id === userId) + 1;
        Logger.log("Index is "+ind+"number"+this.number());
        if (ind === this.number()){
            ind = 0;
        }
        return this.users[ind];
    }

    number() {
        return this.users.length;
    }

    getByKey(key) {
        return this.users[key] ? this.users.key : false;
    }

    add(user) {
        this.users.push(user);
    }

    removeById(userId) {
        this.users = this.users.filter(user => user.id !== userId);
    }

    updateById(user) {

    }

    first(){
        return this.users[0];
    }
}

module.exports = UserStorage;
