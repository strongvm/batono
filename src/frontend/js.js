const windowWidth = document.documentElement.clientWidth;
const windowHeight = document.documentElement.clientHeight;
const isChrome = !!window.chrome && !!window.chrome.webstore;
let runner;
let starthandler;

window.onload = function() {
    const socket = io();
    const some = document.getElementById('some');

    // if (isChrome) {
    //     some.classList.remove('hidden');
    // }

    socket.on('imneedsize', function(msg) {
        socket.emit('mysize', { width: windowWidth,  height: windowHeight });
    });


    // document.onmousedown = function(e) {
    //     if (e.target && e.target.id === 'some') {
    //         var coords = getCoords(some);
    //         var shiftX = e.pageX - coords.left;
    //
    //         document.onmousemove = function(e) {
    //             some.style.left = e.pageX - shiftX + 'px';
    //
    //             if (some.getBoundingClientRect().left <= 0) {
    //                 socket.emit('wentOverTheEdge', { type: 'left', c: some.getBoundingClientRect().left });
    //             }
    //
    //             document.body.onmouseleave = function(event) {
    //                 console.log('mouseleave');
    //                 document.onmousemove = null;
    //             };
    //         };
    //
    //         document.onmouseup = function() {
    //             document.onmousemove = null;
    //             console.log('move stop');
    //         };
    //     }
    // };

    let windowWidth = window.innerWidth;

    some.onclick = function(e) {
        socket.emit('click');
    };

    socket.on('show', function(msg) {
        some.classList.remove('hidden');
        some.classList.remove('hide');
        some.style.left = (- 300 + msg.c) + 'px';
        if(msg.c !== undefined){
            // starthandler = setTimeout(run, 20);
            if(msg.c > 297 && msg.c < 301){
                run();
            }
        }
    });

    socket.on('hide', function(msg) {
        some.classList.add('hide');
        some.classList.add('hidden');
    });

    socket.on('count', function(msg) {
    });

    socket.on('start', function(msg) {
        some.classList.remove('hidden');
        some.classList.remove('hide');
        some.style.left = '0px';
        run();
    });

    function run(){
        runner = setInterval(move, 10);
    }

    function move() {
        // clearTimeout(starthandler);
        some.style.left = some.style.left ? parseInt(some.style.left, 10) + 3 + 'px' : '1px';
        if (some.getBoundingClientRect().right >= windowWidth && some.getBoundingClientRect().left <= windowWidth) {
            socket.emit('wentOverTheEdge', { c: some.getBoundingClientRect().right - windowWidth });
        }
        if (some.getBoundingClientRect().left >= windowWidth){
            clearInterval(runner);
        }
    }

};

function getCoords(elem) {   // кроме IE8-
    var box = elem.getBoundingClientRect();
    return {
        top: box.top + pageYOffset,
        left: box.left + pageXOffset
    };
}
